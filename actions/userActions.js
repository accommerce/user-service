const { Users, Shops, Products, Connection } = require('../models/models')
const Bluebird = require('bluebird')
const { CustomError } = require('accommerce-helpers')

exports.createUser = async (args) => {
    const { password, ...rest } = args

    const user = await Users.register(rest, password)

    const createdUser = await Users.findById(user._id).select('-hash -salt -__v').lean()

    return createdUser
}

exports.updateUser = async (id, args) => {
    const updatedUser = await Users.findByIdAndUpdate(id, { $set: args }, { new: true })
        .select('-hash -salt -__v')
        .lean()

    return updatedUser
}

exports.changePassword = async (id, args) => {
    const user = await Users.findById(id)

    if (!user) {
        throw CustomError(
            `Người dùng với id: ${id} không tồn tại`,
            `Could not find user ${id}`,
            404
        )
    }

    const { oldPassword, newPassword } = args

    await user.changePassword(oldPassword, newPassword)

    return true
}

exports.getUsers = async (args) => {
    const { page: vPage = 1, limit: vLimit = 50, ...rest } = args

    const page = parseInt(vPage)
    const limit = parseInt(vLimit)
    const skip = (page - 1) * limit

    const query = { ...rest, deletedAt: null }

    const [users, total] = await Bluebird.all([
        Users.find(query).select('-hash -salt -__v').skip(skip).limit(limit).lean(),
        Users.countDocuments(query),
    ])

    const pages = Math.ceil(total / limit)

    return { users, total, pages, page }
}

exports.getUserById = async (id) => {
    const user = await Users.findById(id).select('-hash -salt -__v').lean()

    if (!user) {
        throw CustomError(
            `Người dùng với id: ${id} không tồn tại`,
            `Could not find user ${id}`,
            404
        )
    }

    return user
}

exports.deleteUserById = async (id) => {
    const session = await Connection.startSession()
    try {
        session.startTransaction()

        const user = await Users.findOneAndUpdate(
            { _id: id, deletedAt: null },
            { $set: { deletedAt: new Date() } },
            { new: true, session }
        )
            .select('-hash -salt -__v')
            .lean()

        if (!user) {
            throw CustomError(
                `Người dùng với id: ${id} không tồn tại`,
                `Could not find user ${id}`,
                404
            )
        }

        const shops = await Shops.find({ seller: id, deletedAt: null }).select('_id').lean()
        if (shops.length) {
            const shopIds = shops.map((shop) => shop._id)

            await Bluebird.all([
                Shops.updateMany(
                    { _id: { $in: shopIds } },
                    { $set: { deletedAt: new Date() } },
                    { session }
                ),
                Products.updateMany(
                    { shop: { $in: shopIds }, deletedAt: null },
                    { $set: { deletedAt: new Date() } },
                    { session }
                ),
            ])
        }

        await session.commitTransaction()

        return user
    } catch (error) {
        await session.abortTransaction()
        throw error
    } finally {
        await session.endSession()
    }
}

exports.updateUserActiveStatusById = async (id, isActive) => {
    const user = await Users.findByIdAndUpdate(id, { $set: { isActive } }, { new: true })
        .select('-hash -salt -__v')
        .lean()

    if (!user) {
        throw CustomError(
            `Người dùng với id: ${id} không tồn tại`,
            `Could not find user ${id}`,
            404
        )
    }

    return user
}
