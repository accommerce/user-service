const express = require('express')
const router = express.Router()

const userController = require('../controllers/userController')
const { createToken, verifyLogin, verifyRole } = require('../auth/auth')
const {
    validateRegistration,
    validateSelfUpdate,
    validateChangePassword,
} = require('../middleware/validator')

/**
 * Create user
 */
router.post('/', validateRegistration, userController.createUser)

/**
 * GET JWT token with username and passport
 */
router.post('/jwt', async (req, res, next) => {
    try {
        createToken(req, res, next)
    } catch (error) {
        next(error)
    }
})

/**
 * All API bellow need authentication
 */
router.use(verifyLogin)

/**
 * GET current user info
 */
router.get('/self', userController.getCurrentUser)

/**
 * EDIT current user info
 */
router.put('/self', validateSelfUpdate, userController.updateCurrentUser)

/**
 * CHANGE current user password
 */
router.put('/self/password', validateChangePassword, userController.changePassword)

/**
 * All API bellow need authorized as Admin
 */

router.use(verifyRole('admin'))

/**
 * GET users
 */
router.get('/', userController.getUsers)

/**
 * GET user by id
 */
router.get('/:id', userController.getUserById)

/**
 * DELETE user by id
 */
router.delete('/:id', userController.deleteUserById)

/**
 * DEACTIVATE/ACTIVATE user by id
 */
router.put('/:id', userController.updateUserActiveStatusById)

module.exports = router
