const userActions = require('../actions/userActions')
const sendResponse = require('../helpers/sendResponse')
const { CustomError } = require('accommerce-helpers')

exports.createUser = async (req, res, next) => {
    try {
        const args = req.body
        const user = await userActions.createUser(args)

        sendResponse(
            user,
            'Đăng ký tài khoản thành công',
            'Created user successfully!',
            201
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.getCurrentUser = (req, res, next) => {
    try {
        const user = req.user || {}

        sendResponse(user)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.updateCurrentUser = async (req, res, next) => {
    try {
        const args = req.body
        const id = req.user._id
        const updatedUser = await userActions.updateUser(id, args)

        sendResponse(updatedUser)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.changePassword = async (req, res, next) => {
    try {
        const args = req.body
        const id = req.user._id
        await userActions.changePassword(id, args)

        sendResponse(true, 'Thay đổi mật khẩu thành công', 'Change password successfully!')(
            req,
            res,
            next
        )
    } catch (error) {
        next(error)
    }
}

exports.getUsers = async (req, res, next) => {
    try {
        const args = req.query
        const users = await userActions.getUsers(args)

        sendResponse(users)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.getUserById = async (req, res, next) => {
    try {
        const { id } = req.params
        const user = await userActions.getUserById(id)

        sendResponse(user)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.deleteUserById = async (req, res, next) => {
    try {
        const { id } = req.params
        const user = await userActions.deleteUserById(id)

        sendResponse(
            user,
            `Xóa người dùng ${user.username} thành công`,
            `Deleted user ${user.username}`
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.updateUserActiveStatusById = async (req, res, next) => {
    try {
        const { id } = req.params
        const { isActive } = req.body

        if (typeof isActive !== 'boolean') {
            throw CustomError(
                'Có lỗi xảy ra. Vui lòng thử lại sau!',
                'Parameter isActive must be a boolean',
                400
            )
        }

        const user = await userActions.updateUserActiveStatusById(id, isActive)

        sendResponse(
            user,
            `Thay đổi trạng thái kích hoạt của người dùng ${user.username} thành công`,
            `Change user ${user.username} active status successfully`
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}