const Joi = require('joi')
const Bluebird = require('bluebird')
const { Users } = require('../models/models')
const { CustomError } = require('accommerce-helpers')

const newUserSchema = Joi.object({
    username: Joi.string().alphanum().trim().min(3).max(30).required(),
    password: Joi.string().min(8).required(),
    firstName: Joi.string().trim().max(100).required(),
    lastName: Joi.string().trim().max(100).required(),
    phoneNumber: Joi.string()
        .pattern(new RegExp('^([+84|84|0]+([35789]))+([0-9]{8})$', 's'))
        .required(),
    email: Joi.string().email(),
    address: Joi.string().trim().max(255),
})

exports.validateRegistration = async (req, res, next) => {
    try {
        const { error, value } = newUserSchema.validate(req.body)

        if (error) {
            error.status = 400
            return next(error)
        }

        const { username, phoneNumber } = value

        const { username: usernameExist, phoneNumber: phoneNumberExist } =
            (await Users.findOne({
                $or: [{ username }, { phoneNumber }],
            }).lean()) || {}

        if (usernameExist === username) {
            throw CustomError(
                `Người dùng với tên đăng nhập: ${username} đã tồn tại`,
                `User with username ${username} already exists!`,
                400
            )
        }

        if (phoneNumberExist === phoneNumber) {
            throw CustomError(
                `Người dùng với số điện thoại: ${phoneNumber} đã tồn tại`,
                `User with phone number ${phoneNumber} already exists!`,
                400
            )
        }

        req.body = value
        next()
    } catch (error) {
        next(error)
    }
}

const selfUpdateUserSchema = Joi.object({
    firstName: Joi.string().trim().max(100),
    lastName: Joi.string().trim().max(100),
    phoneNumber: Joi.string().pattern(new RegExp('^([+84|84|0]+([35789]))+([0-9]{8})$', 's')),
    email: Joi.string().email(),
    address: Joi.string().trim().max(255),
})

exports.validateSelfUpdate = async (req, res, next) => {
    try {
        const { error, value } = selfUpdateUserSchema.validate(req.body)

        if (error) {
            error.status = 400
            return next(error)
        }

        const { phoneNumber } = value

        const phoneNumberExist = await Users.findOne({
            _id: { $ne: req.user._id },
            phoneNumber,
        }).lean()

        if (phoneNumberExist) {
            throw CustomError(
                `Người dùng với số điện thoại: ${phoneNumber} đã tồn tại`,
                `User with phone number ${phoneNumber} already exists!`,
                400
            )
        }

        req.body = value
        next()
    } catch (error) {
        next(error)
    }
}

const changePasswordSchema = Joi.object({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().min(8).required(),
})

exports.validateChangePassword = (req, res, next) => {
    try {
        const { error, value } = changePasswordSchema.validate(req.body)

        if (error) {
            error.status = 400
            return next(error)
        }

        req.body = value
        next()
    } catch (error) {
        next(error)
    }
}
